FROM node:boron

RUN mkdir -p /home/app
WORKDIR /home/app

COPY . /home/app

RUN npm install --quiet
RUN npm install -g pm2@2.6.1 babel-cli --quiet
RUN npm run build

ENV PORT $PORT
ENV NODE_ENV $NODE_ENV
ENV SECRET_KEY $SECRET_KEY
ENV MONGODB_URI $MONGODB_URI

CMD npm run start
