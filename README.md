**Spaces Backend RESTful API Backend**
===================================

Getting Started
=================
**Requirements**
  - docker *ver. 17.06.0-ce*
  - docker-compose *ver. 1.14.0*

1. Clone this Repository
2. Run:
  - `cd mcs2-be`
  - `docker-compose build`
  - `make install`
  - `docker-compose up` or `docker-compose up -d` (demonize)
  - to follow logs when demonized `docker-compose logs -f`

  (notice: *`make install` is only required for your* ***first installation*** *or after installation of a* ***new npm package***)

Running Tests and Checks
========================
  1. Run tests inside container:
  - run once: `docker-compose run app bash -c 'npm run test'`
  - run and watch for file changes: `docker-compose run app bash -c 'npm run test:watch'`
  
  2. Run code linting inside container:
  - fix code style: `docker-compose run app bash -c 'npm run lint:fix'`
  - run lint to check: `docker-compose run app bash -c 'npm run lint'`
  

Now you can start coding ... :)
