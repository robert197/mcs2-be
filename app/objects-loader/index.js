import fs from 'fs'
import path from 'path'

function filesFromSrcFolder () {
  return fs.readdirSync(path.join(__dirname, '../src/'))
}

function isController (file) {
  const isTestFile = (file.slice(-8) === '.test.js')
  const isJsFile = (file.slice(-3) === '.js')
  const isHiddenFile = (file.indexOf('.') === 0)
  const isControllerFile = (file.slice(-14) === '.controller.js')
  return !isHiddenFile &&
    !isTestFile &&
    isJsFile &&
    isControllerFile
}

export default (server, io) => {
  filesFromSrcFolder().filter(firstLevelFile => {
    fs.readdirSync(path.join(__dirname, '../src/', firstLevelFile))
      .filter(isController)
      .forEach(file => {
        const route = require(path.join(__dirname, '../src/' + firstLevelFile + '/' + file))
        route.default(server, io)
        console.log(`"${file.slice(0, -3)}" registered.`)
      })
  })
}
