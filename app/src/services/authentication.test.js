import { authentication } from './authentication'
import sinon from 'sinon'
import jwt from 'jsonwebtoken'
const secret = process.env.SECRET_KEY || 'test-key-123'

describe('Generating token', () => {
  it('should return token with message and status when token generated successfully', () => {
    const sign = sinon.stub(jwt, 'sign')
    const unit = {
      '_id': '5a9c248d64d3401d6539a2c6',
      'name': 'unitname 2',
      'capacity': 3,
      'description': 'asdaisdn',
      'emergency': '5a91be3de9d1570c0079e45e',
      'status': {
        'name': 'ON_THE_WAY'
      }
    }
    sign.withArgs({ unit }, secret, { expiresIn: '24h' }).returns('abc')
    const generatedResponse = authentication.generateTokenResponse({ unit })
    sinon.assert.match(generatedResponse, {
      success: true,
      token: 'abc'
    })
    sign.restore()
  })

  it('should return error message when token could not be generated', () => {
    const sign = sinon.stub(jwt, 'sign')
    sign.withArgs({unitId: '38fdfsn38cn'}, secret, { expiresIn: '24h' }).returns(undefined)
    const generatedResponse = authentication.generateTokenResponse('38fdfsn38cn')
    sinon.assert.match(generatedResponse, {
      success: false,
      token: undefined
    })
    sign.restore()
  })
})

describe('Veryfying token', () => {
  it('should throw error when no token was given', () => {
    authentication.verifyToken(undefined, (err, decoded) => {
      sinon.assert.match(err.message, 'No token provided')
    })
  })

  it('should throw error when veryfying token failed', () => {
    authentication.verifyToken('token', (err, decoded) => {
      sinon.assert.match(err.message, 'Failed to authenticate token')
    })
  })

  it('should send decoded number when veryfying token was successfull', () => {
    authentication.verifyToken(jwt.sign({}, secret), (err, decoded) => {
      if (err) {
        throw new Error(err)
      }
      sinon.assert.match(typeof decoded.iat, 'number')
    })
  })
})
