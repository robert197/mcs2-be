import jwt from 'jsonwebtoken'
const secret = process.env.SECRET_KEY || 'test-key-123'

const authentication = {

  /**
   * Returns a token and status when token generation was successfull.
   * @param {Unit} unit
   * @returns {Object}
   */
  generateTokenResponse (unit) {
    const token = jwt.sign({ unit }, secret, { expiresIn: '24h' })
    return {
      success: !!token,
      token
    }
  },

  /**
   * Veryfies given token and calls callback with payload of token or an error.
   *
   * @param {String} token
   * @param {function} cb
   */
  verifyToken (token, cb) {
    if (!token) {
      cb(new Error('No token provided'), null)
      return
    }
    jwt.verify(token, secret, (err, decoded) => {
      if (err) {
        cb(new Error('Failed to authenticate token'), null)
      } else {
        cb(null, decoded)
      }
    })
  }
}

export { authentication }
