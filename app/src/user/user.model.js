import { mongoose } from '../../connections/mongo'
import bcrypt from 'bcrypt-nodejs'

const UserSchema = new mongoose.Schema({
  username: { type: String, required: true, unique: true },
  password: { type: String, required: true },
  name: String,
  unit: {type: String, default: '5a9c248d64d3401d6539a2c6'}
})

UserSchema.pre('save', function (next) {
  const user = this
  if (user.isModified('password') || user.isNew) {
    bcrypt.genSalt(10, function (err, salt) {
      if (err) {
        return next(err)
      }
      bcrypt.hash(user.password, salt, null, function (err, hash) {
        if (err) {
          return next(err)
        }
        user.password = hash
        next()
      })
    })
  } else {
    return next()
  }
})

UserSchema.methods = {

  /**
   * Compoares password and returns boolean
   * @param {String} password
   * @returns {boolean}
   */
  comparePassword (password) {
    return bcrypt.compareSync(password, this.password)
  }
}

export default mongoose.model('User', UserSchema)
