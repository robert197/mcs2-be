import User from './user.model'

export default {

  /**
   * Queries a user and compares passwords.
   *
   * @param {number} username
   * @param {number} password
   * @returns {User | null}
   */
  findByUsernameAndPassword (username, password) {
    return User.find({ 'username': username })
        .then((user) => {
          return user.length === 1 && user[0].comparePassword(password) ? user[0] : null
        })
  },

  /**
   * Creates a new user. Username has to be unique.
   *
   * @param {String} username
   * @param {String} password
   */
  createNewUser (username, password) {
    const user = new User({ username, password })
    return user.save()
  },

  /**
   * Removes all users.
   *
   * @returns {Promise}
  */
  removeAllUsers () {
    return User.remove({})
  },

  /**
   * Returns promise with users array.
   *
   * @returns {Promise <User[]>}
  */
  findAllUsers () {
    return User.find({})
  },

  /**
   * Deletes one user.
   *
   * @param {number} id
   */
  deleteUser (id) {
    return User.deleteOne(id)
  }
}
