import UserRepository from './user.repository'
import UnitRepository from '../unit/unit.repository'
import { authentication } from '../services/authentication'

export default (server) => {
  server.post('/login', login)
  server.post('/user', user)
  server.post('/down', down)
  server.get('/users', users)
  server.del('/user/:id', deleteUser)
}

/**
 * Logs in given user. Requires keys: username and password inside body of request.
 *
 * @param {Request} req
 * @param {Response} res
 */
function login (req, res) {
  UserRepository.findByUsernameAndPassword(req.body.username, req.body.password)
    .then(user => {
      if (user) {
        UnitRepository.getUnit(user.unit)
        .then((unit) => {
          res.send(authentication.generateTokenResponse(unit))
        })
        .catch((err) => res.send(err))
      } else {
        res.send({success: false, message: 'Authentication failed'})
      }
    })
    .catch((err) => {
      res.send(err)
    })
}

/**
 * Creates new user. Requires keys: username and password inside body of request.
 *
 * @param {Request} req
 * @param {Response} res
 */
function user (req, res) {
  UserRepository.createNewUser(req.body.username, req.body.password)
    .then((user) => {
      res.send(user)
    })
    .catch((err) => {
      res.send(err)
    })
}

/**
 * Removes all users from db.
 *
 * @param {Request} req
 * @param {Response} res
 */
function down (req, res) {
  UserRepository.removeAllUsers()
    .then(() => {
      res.send('Removed all users')
    })
    .catch((err) => {
      res.send(err)
    })
}

/**
 * Sends all users from db.
 *
 * @param {Request} req
 * @param {Response} res
 */
function users (req, res) {
  UserRepository.findAllUsers()
    .then((users) => {
      res.send(users)
    })
    .catch((err) => {
      res.send(err)
    })
}

/**
 * Removes specific user and returns object with status.
 *
 * @param {Request} req
 * @param {Response} res
 */
function deleteUser (req, res) {
  UserRepository.deleteUser(req.params.id)
    .then((deleted) => {
      res.send(deleted)
    })
    .catch(err => {
      res.send(err)
    })
}
