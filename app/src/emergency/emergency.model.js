import { mongoose } from '../../connections/mongo'

const Media = new mongoose.Schema({
  type: Number,
  data: String
})

const Proofs = new mongoose.Schema({
  type: Number,
  Media: [Media]
})

const Emergency = new mongoose.Schema({
  name: String,
  requiredUnits: {type: Number, min: 1},
  description: String,
  status: {
    name: String
  },
  type: {
    name: String
  },
  createdAt: {type: Date, default: Date.now},
  location: {
    lat: Number,
    long: Number,
    city: String,
    street: String,
    streetNr: String,
    zip: Number
  },
  overtakenUnits: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Unit'
  }],
  arrivedUnits: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Unit'
  }],
  proofs: [Proofs]
})

export default mongoose.model('Emergency', Emergency)
