import io from 'socket.io-client'
import sinon from 'sinon'

const ioOptions = {
  transports: ['websocket'],
  forceNew: true,
  reconnection: false,
  query: {
    Authorization: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bml0SWQiOiI1YTliM2I2MzEzODU3YzFhOTJmOGRlNjYiLCJpYXQiOjE1MjA3NzAyOTUsImV4cCI6MTUyMDg1NjY5NX0.sf4UTg1y4d2VtUORt_mHV6dn3ar35e5kihlCl4-ddJQ'
  }
}
let sender

describe('socket', () => {
  beforeEach((done) => {
    sender = io.connect('http://localhost:8000', ioOptions)
    setTimeout(() => {
      done()
    }, 1200)
  })
  it('should receive new emergency with overtaken status', (done) => {
    const expectedUnit = {
      _id: '5a9c248d64d3401d6539a2c6',
      capacity: 3,
      crew: [],
      description: 'asdaisdn',
      emergency: '5a91be3de9d1570c0079e45e',
      name: 'unitname 2',
      status: { name: 'ON_THE_WAY' }
    }

    sender.emit('overtake_emergency', '5a91be3de9d1570c0079e45e')
    sender.on('emergency_overtaken', (actualUnit) => {
      sinon.assert.match(actualUnit, expectedUnit)
      done()
    })
  })
})
