import Emergency from './emergency.model'

export default {

  /**
   * Returns emergency by specific id
   *
   * @param {number} id
   * @returns {Promise<Emergency>}
   */
  getEmergencyById (id) {
    return Emergency.findById(id)
  },

  /**
   * Returns all emergencies
   *
   * @returns {Promise<Emergency[]>}
  */
  getAllEmergencies () {
    return Emergency.find({})
  },

  /**
   * Creates an emergency
   *
   * @returns {Promise}
  */
  createEmergency (emergency) {
    return (new Emergency(emergency)).save()
  },

  /**
   * Overtakes emergency by adding unitId to overtakingEmergency array,
   * decreasing requiredUnits and setting status to OVERTAKEN_EMERGENCY.
   *
   * @param {number} emergencyId
   * @param {number} unitId
   * @return {Promise<Emergency>}
   */
  overtakeEmergency (emergencyId, unitId) {
    return Emergency.update({_id: emergencyId}, {
      $push: { overtakenUnits: unitId },
      $inc: {requiredUnits: -1},
      $set: {
        status: {
          name: 'OVERTAKEN_EMERGENCY'
        }
      }
    }, {new: true})
  }
}
