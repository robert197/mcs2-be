import EmergencyRepository from './emergency.repository'
import UnitRepository from '../unit/unit.repository'
let socket
let io

export default (server, socketIo) => {
  server.get('/emergencies', getEmergencies)
  server.get('/emergency', getEmergency)
  server.post('/emergency', createEmergency)
  if (socketIo) {
    socket = socketIo.socket
    io = socketIo.io
    socket.on('overtake_emergency', onOvertakeEmergency.bind({unitId: socket.unitId}))
  }
}

/**
 * Creates a defined emergency.
 *
 * @param {Request} req
 * @param {Response} res
 */
function createEmergency (req, res) {
  EmergencyRepository.createEmergency(req.body)
    .then(emergency => {
      res.send(emergency)
    }).catch(err => {
      res.send(err)
    })
}

/**
 * Retrieves all emergencies.
 *
 * @param {Request} req
 * @param {Response} res
 */
function getEmergencies (req, res) {
  EmergencyRepository.getAllEmergencies(req.query.id)
    .then(emergencies => {
      res.send(emergencies)
    }).catch(err => {
      res.send(err)
    })
}

/**
 * Retrieves an emergency. The url should be for e.g. /emergency?id=1.
 *
 * @param {Request} req
 * @param {Response} res
 */
function getEmergency (req, res) {
  EmergencyRepository.getEmergencyById()
    .then(emergency => {
      res.send(emergency)
    }).catch(err => {
      res.send(err)
    })
}

/**
 * Updates status of unit to ON_THE_WAY.
 * Overtakes emergency by passing a unit to overtakenUnits and decreases required units.
 * Emits 'emergency_overtaken' socket event to notify all clients about this.
 *
 * @param {number} emergencyId
 */
function onOvertakeEmergency (emergencyId) {
  console.log('asdadasd')
  UnitRepository.updateStatusOnOvertakingEmergency(this.unitId, emergencyId)
    .then((unit) => {
      EmergencyRepository.overtakeEmergency(emergencyId, this.unitId)
      .then(() => {
        io.sockets.emit('emergency_overtaken', unit)
      })
      .catch((err) => {
        io.sockets.emit('emergency_overtaken', err)
      })
    })
}
