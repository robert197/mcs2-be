import { authentication } from '../services/authentication'

export default {
  http: {
    routesToIgnore: [],
    /**
     * Sets routes to ignore to accessable variable
     *
     * @param {Array} routes
     */
    ignoreRoutes (...routes) {
      this.routesToIgnore = routes
      return this
    },
    /**
     * Verifies a token placed inside http header and assigns decoded payload to request
     *
     * @return {Function}
    */
    verify () {
      return (req, res, next) => {
        const reqestPath = req.path().toLowerCase()
        if (this.routesToIgnore.indexOf(reqestPath) >= 0) {
          return next()
        }
        authentication.verifyToken(req.header('Authorization'), (err, decoded) => {
          if (decoded && !err) {
            req.decoded = decoded
            next()
          } else {
            res.send({success: false, message: err.message})
          }
        })
      }
    }
  },
  socket: {
    /**
     * Verifies token placed inside socket header and assigns decoded unitId to socket
     *
     * @return {Function}
     */
    verify () {
      return (socket, next) => {
        authentication.verifyToken(socket.handshake.query['Authorization'], (err, decoded) => {
          if (decoded && !err) {
            socket.unitId = decoded.unit._id
            return next()
          }
          return next('Authentication error! Missing or wrong auth token')
        })
      }
    }
  }
}
