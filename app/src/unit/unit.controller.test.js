import io from 'socket.io-client'
import sinon from 'sinon'

const ioOptions = {
  transports: ['websocket'],
  forceNew: true,
  reconnection: false,
  query: {
    Authorization: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bml0SWQiOiI1YTliM2I2MzEzODU3YzFhOTJmOGRlNjYiLCJpYXQiOjE1MjA3NzAyOTUsImV4cCI6MTUyMDg1NjY5NX0.sf4UTg1y4d2VtUORt_mHV6dn3ar35e5kihlCl4-ddJQ'
  }
}
let sender

const expectedUnit = {
  'name': 'Test Unit',
  'capacity': 2,
  'location': {
    'long': 2.3213213,
    'lat': 4.321324
  }
}

describe('socket', () => {
  beforeEach((done) => {
    sender = io.connect('http://localhost:8000', ioOptions)
    setTimeout(() => {
      done()
    }, 1200)
  })

  it('should emit location_update event when location of unit changes', (done) => {
    sender.emit('location_update', expectedUnit)
    sender.on('location_update', actualUnit => {
      sinon.assert.match(actualUnit, expectedUnit)
      done()
    })
  })
})
