import { mongoose } from '../../connections/mongo'

export default mongoose.model('Unit', {
  name: {type: String, required: true},
  capacity: {type: Number, required: true},
  description: String,
  status: {
    name: {type: String, default: 'FREE_UNIT'}
  },
  location: {
    long: Number,
    lat: Number,
    country: String,
    city: String,
    street: String,
    streetNr: String,
    zip: Number
  },
  crew: {
    type: [mongoose.Schema.Types.ObjectId],
    ref: 'Crew'
  },
  emergency: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Emergency'
  }
})
