import Unit from './unit.model'

export default {

  /**
   * Updates an units status to ON_THE_WAY and sets the id of overtaken emergency.
   *
   * @param {number} unitId
   */
  updateStatusOnOvertakingEmergency (unitId, emergencyId) {
    return Unit.findOneAndUpdate(unitId, {
      $set: {
        status: {
          name: 'ON_THE_WAY'
        },
        emergency: emergencyId
      }
    })
  },

  /**
   * Creates a new unit.
   *
   * @param {Unit} unit
   */
  createUnit (unit) {
    return (new Unit(unit)).save()
  },

  /**
   * Updates Unit object.
   *
   * @param {Unit} unit
   */
  updateUnit (unit) {
    return Unit.findByIdAndUpdate(unit.id, {$set: unit}, {new: true})
  },

  /**
   * Returns promise with array of all units.
   *
   * @returns {Promise<Unit[]>}
  */
  getAllUnits () {
    return Unit.find({})
  },

  /**
   * Returns promise with unit.
   *
   * @param {String} id
   * @returns {Promise<Unit>}
   */
  getUnit (id) {
    return Unit.findById(id)
  },

  /**
   * Deleted one unit.
   * @param {number} id
   */
  deleteUnit (id) {
    return Unit.deleteOne(id)
  }
}
