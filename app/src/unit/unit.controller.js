import UnitRepository from './unit.repository'
let socket
let io

export default (server, socketIo) => {
  server.get('/units', getUnits)
  server.get('/unit', getUnit)
  server.post('/unit', createUnit)
  server.del('/unit/:id', deleteUnit)
  server.put('/unit', updateUnit)
  if (socketIo) {
    io = socketIo.io
    socket = socketIo.socket
    socket.on('location_update', onLocationUpdate)
  }
}

/**
 * Creates new unit. Requires a unit object
 *{
 * name: String,
 * capacity: Number
 *}
 *
 * @param {Request} req
 * @param {Response} res
 */
function createUnit (req, res) {
  UnitRepository.createUnit(req.body)
    .then(unit => {
      res.send(unit)
    })
    .catch(err => {
      res.send(err)
    })
}

/**
 * Updates a unit. Id inside unit object is required!
 *
 * @param {Request} req
 * @param {Response} res
 */
function updateUnit (req, res) {
  UnitRepository.updateUnit(req.body)
    .then(unit => {
      res.send(unit)
    })
    .catch(err => {
      res.send(err)
    })
}

/**
 * Responses all units.
 *
 * @param {Request} req
 * @param {Response} res
 */
function getUnits (req, res) {
  UnitRepository.getAllUnits()
    .then(units => {
      res.send(units)
    })
    .catch(err => {
      res.send(err)
    })
}

/**
 * Responses a unit with requested id. e.g. /unit?id=anyidgoeshere
 *
 * @param {Request} req
 * @param {Response} res
 */
function getUnit (req, res) {
  UnitRepository.getUnit(req.query.id)
    .then(unit => {
      res.send(unit)
    })
    .catch(err => {
      res.send(err)
    })
}

/**
 * Deletes specific unit and returns object with status.
 *
 * @param {Request} req
 * @param {Response} res
 */
function deleteUnit (req, res) {
  UnitRepository.deleteUnit(req.params.id)
    .then((deleted) => {
      res.send(deleted)
    })
    .catch(err => {
      res.send(err)
    })
}

/**
 * Broadcasts an update of units location to all users.
 *
 * @param {Unit} unit
 */
function onLocationUpdate (unit) {
  console.log(JSON.parse(unit).location)
  io.sockets.emit('location_update', JSON.parse(unit))
}
