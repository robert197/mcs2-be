import { mongoose } from '../../connections/mongo'

export default mongoose.model('Type', {
  name: String
})
