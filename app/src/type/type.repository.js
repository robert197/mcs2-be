import Type from './type.model'

export default {

  /**
   * Returns promisse with types array.
   *
   * @returns {Promise<Type[]>}
  */
  getAllTypes () {
    return Type.find({})
  },

  /**
   * Creates new type.
   *
   * @param {Type} type
   */
  createType (type) {
    return (new Type(type)).save()
  }
}
