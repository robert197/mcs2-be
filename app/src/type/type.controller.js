import TypeRepository from './type.repository'

export default (server) => {
  server.post('/type', addType)
  server.get('/types', getTypes)
}

/**
 * Sends all types from db.
 *
 * @param {Request} req
 * @param {Response} res
 */
function getTypes (req, res) {
  TypeRepository.getAllTypes()
    .then(types => {
      res.send(types)
    })
    .catch(err => {
      res.send(err)
    })
}

/**
 * Creates type. Requires type object in body of request.
 * {
 *  name: String
 * }
 *
 * @param {Request} req
 * @param {Response} res
 */
function addType (req, res) {
  TypeRepository.createType(req.body)
    .then(type => {
      res.send(type)
    }).then(err => {
      res.send(err)
    })
}
