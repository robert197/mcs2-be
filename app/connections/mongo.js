import mongoose from 'mongoose'
const mongoUri = process.env.MONGODB_URI

mongoose.init = () => {
  mongoose.connect(mongoUri, { useMongoClient: true, promiseLibrary: global.Promise })
}

export { mongoose }
