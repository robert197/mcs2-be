import 'babel-register'
import restify from 'restify'
import bodyParser from 'body-parser'
import loader from './objects-loader'
import { mongoose } from './connections/mongo'
import SocketIO from 'socket.io'
import authChecker from './src/midlewares/authChecker'

console.log(`ENVIRONMENT: ${process.env.NODE_ENV}`)
const port = process.env.PORT || 3030
const server = restify.createServer({
  name: 'spaces-node-1'
})
const io = new SocketIO(server.server)
const corsMiddleware = require('restify-cors-middleware')
const cors = corsMiddleware({
  origins: ['*'],
  allowHeaders: ['*']
})

server.pre(cors.preflight)
server.pre(restify.pre.sanitizePath())
server.use(cors.actual)
server.use(restify.plugins.fullResponse())
server.use(bodyParser.json())
server.use(restify.plugins.queryParser())
server.use(authChecker.http.ignoreRoutes('/', '/login', '/user').verify())
// io.use(authChecker.socket.verify())

mongoose.init()
loader(server)

io.on('connection', (socket) => {
  console.log('Socket connected')
  loader(server, {socket, io})
})

server.listen(port, () => {
  console.log(`Server listening at: http://127.0.0.1:${port}/`)
})

export default server
